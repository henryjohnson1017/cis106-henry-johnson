#This program reads data from txtfile.txt and creates an array of the grades.  With that array, it displays the contents
#then calculates the high, low, and average score.


#This function reads the file and creates an array of the scores
def get_grades():
    filename = "txtfile.txt"
    grades_list = []
    with open(filename) as f:
        content = f.readlines()
        for line in content:
            split_content = line.split(" ")
            grades_list.append(int(split_content[2]))
        return grades_list

def average_grade(grades_list):
    average = sum(grades_list)/len(grades_list)
    return average
    
def max_grade(grades_list):
    highest = max(grades_list)
    return highest
    
def min_grade(grades_list):
    lowest = min(grades_list)
    return lowest
    
def display_sp_scores(average, highest, lowest):
    print(average)
    print(highest)
    print(lowest)
    
def display_grades_list(grades_list):
    print(grades_list)

def main():
    grades_list = get_grades()
    average = average_grade(grades_list)
    highest = max_grade(grades_list)
    lowest = min_grade(grades_list)
    display_grades_list(grades_list)
    display_sp_scores(average, highest, lowest)
    
main()