//This javaScript code uses fuctions to calculate gross pay
var hours, rate, gross_pay;

/**
 * main program to execute the functions
 */
function main() {
  hours = get_hours();
  rate = get_rate();
  calculate_pay(hours, rate);
}

/**
 * this function asks the user for how many hours they worked
 */
function get_hours() {
  hours = parseFloat(window.prompt('How many hours have you worked?'));
  return hours;
}

/**
 * this function asks the user or their hourly rate
 */
function get_rate() {
  rate = parseFloat(window.prompt('What is your hourly rate?'));
  return rate;
}

/**
 * this function calculates and displays the user's gross pay
 */
function calculate_pay(hours, rate) {
  gross_pay = rate * hours;
  window.alert(String('Your gross pay is $') + String(gross_pay));
}


main();