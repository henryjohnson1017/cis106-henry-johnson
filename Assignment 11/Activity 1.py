# This program asks a user for grade scores, calculating the average score, maximum score, and minimum score and prints all scores as a list.

from numbers import Number

def number_prompt():
    number_scores = float(input('How many scores would you like to average? '))
    return number_scores
  

def while_loop(number_scores, list_a):
    count = 0
    running_total = 0
    while count < number_scores:
        score = float(input('What is your score? '))
        list_a.append(score)
        running_total = running_total + score
        count = (count if isinstance(count, Number) else 0) + 1
    return running_total
  
  
def calculate_score(number_scores, running_total):
    avg_score = running_total / number_scores
    return avg_score
  
  
def display_average_score(avg_score, list_a):
    print(str('Your average score is ') + str(avg_score))  
    print(str('Your max score is ') + str(max(list_a)))
    print(str('Your min score is ') + str(min(list_a)))
    print(list_a)
  
  
def main():
   number_scores = number_prompt()
   running_total = while_loop(number_scores, list_a)
   avg_score = calculate_score(number_scores, running_total)
   display_average_score(avg_score, list_a)

  
main()
