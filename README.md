# CIS106 - Henry Johnson

# Session 1

My name is Henry Johnson and I would like to become more fluent in computer programming.
I chose JavaScript because I have only used Java and C++ before and using blockly, that was the closest option. I would like to add this to my skill set to have a role in the tech industry, although I am not specialized in any field yet.

# Session 2

In this session, I learned about the process in which information travels when using an IDE. 
Using Git commands taught me about what I need to tell the computer to make this information travel.
I tested out using CodeChef as an IDE for running Java and passed a simple Hello World through there, it was simple but I have not yet learned how I would clone this information over to something like Bitbucket or Github.
I am also using Cloud9; after getting over the initial learning curve, it is a rather user-friendly interface, with everything nearby.
I will use IDEs in my life for programming and compiling personal programs.  

# Session 3

In this session I learned about manipulating user-defined variables. 
Using blockly, it was not obvious to me how to set the user response to a variable so I found the Wikiversity page on variables to be very helpful.
This is important to me because then it creates a program that a user can interact with.
I like to create these types of programs so I see this as a skill I will use for the rest of my programming career.

# Session 4

This taught me how to code in python and what each of the lines I had seen before in blockly meant.
I had to do more reasearch on the internet to find out how to use equations, but it is helpful because everything I need to know is out there somewhere.
I switched over to python from javaScript because it seemed like it was much more straightforward.
A person in the tech industry will use variables in most programs because it allows data to be stored, processed, and manipulated.

# Session 5

This session taught me about using functions and creating code that is easy to read, debug, and add and subtract from.
I had gone to the tutoring center to get help on how to create functions using blockly.
There were no teachers there so I paired up with another student in another section of 106. 
Ironically, she was more lost than I was and I helped her along with her pseudocode, and this helped me get a better understanding of what I was doing and why I was doing it.
Functions break down the code into individual steps, so it makes complicated code easy to understand as it is just a group of simple functions.
This will be helpful when creating intricate programs because it makes creating and debugging them a much less daunting task.

# Session 6

This session solidified my ability to write functions in python.
It was time consuming to put together, but it serves as a foundation for future code.
Functions make it very easy to have organized code that can be manipulated, if necessary.
It also makes it easier for debugging because it moves in a process so it is easy to identify where something went wrong.
I can see using functions in almost every program throughout my programming career because it is a good habit for writing well-organized code.

# Session 7

This session taught me how to create dynamic programs that can react to the variety of inputs a user can throw in.
More complicated programs will often have different paths that a user can go down and conditions allow a user to make a choice other than true or false.
Using conditions allows a program to give an output that is more tailored to the intentions of the user.
I can see myself using conditions in my programming career when creating programs that include more than one function.

# Session 8

This session taught me how to code conditional statements in Python.
I implemented conditional statements into some of the previous activities that we completed.
To program this, I learned to nest my functions within the conditional statement to keep my main function concise.
I learned about how to input user prompts into conditional statements and calculate a response tailored to the statement.
This is useful for creating smarter programs that respond to the user input.

# Session 9

This session taught me about how to use the different types of loops and how to computer reads through loops. 
Some loops are pre-test and some loops are post-test so that is an important detail to pay attention to in regards to the count. 
Loops are useful for minumizing redundancies in code. 
Something can be hard coded multiple times, or it can be passed through a loop as many times as necessary. 
This will be useful when writing more complex code because it reduces the amount of code a programmer has to read or write, and similar to functions, it can be reused and repurposed.

# Session 10

This session taught me how to code loops in Python.
The session proved to be more difficult than previous sessions because it is a cumulative test of every aspect of coding that we have used and will be used. 
Nesting loops can be tricky to conceptualize, but is a very useful skill for writing brief code than can represent the extensive outputs of a function.
This is something that will be used in my entire coding career because once mastered, it will make coding easier to write and manipulate.

# Session 11

This session taught me how to code lists in python.
Lists are helpful for working with data because it allows for the data to be stored in one place.
They also are make it easy to manipulate that data and extract what you are looking for.
Lists and arrays are something I will use in my coding career because it will allow for creating software that will make business life and logistics easier.

# Session 12

This session allowed me to learn more about creating lists in python.
Parallel lists allow for data to be aligned which is helpful for keeping track of related data.
Multidimentional arrays allow for data to be stored in multiple dimensions which is especially helpful with data that has a pattern or for charts.
Dynamic Arrays are helpful for storing data and manipulating the data and always having room to store it.
I will use these types of arrays in my coding career for creating programs that store information that can be manipulated or found easily.

# Session 13

This session taught me how to manipulate strings.
This session will be very beneficial for creating programs that require users to input words and to make data still applicable even if the user does not input data how it is stored.
I will use these string skills for future projects in maintaining a user input database.

# Session 14

This session taught me how to import, read and write files.
This will be beneficial to writing programs that need to take information from outside the program.
This allows me to expand from within the confines of the program because I am able to import or export data from a program.
I will be able to use these skills throughout my programming career and will allow me to write more useful programs for creating and reading databases.

# Session 16

This session taught me how to read and categorize data from an XML file.
It also taught me about manipulating data types and how some functions require specific data types to not boot out an error. 
This program has demonstrated how far I have come as a coder from barely being able to code a "hello world" to being able to complete a complex program.
I will use these skills as I continue through my programming career and it has boosted my confidence as an aspire software developer.
Thank you.