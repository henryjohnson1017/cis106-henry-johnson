##This program wil take a user's age and convert it to their choice of months, days, hours, or seconds.

months = None
days = None
hours = None
seconds = None
age = None
choice = None

def text_prompt(msg):
  try:
    return raw_input(msg)
  except NameError:
    return input(msg)

"""Describe this function...
"""
def main():
  global months, days, hours, seconds, age, choice
  age = float(text_prompt('How old are you in years?'))
  choice = text_prompt('Would you like to convert your age to months(M), days(D), hours(H), or seconds(S)?')
  if choice == 'M' or choice == 'm':
    print_months(convert_to_months())
  elif choice == 'D' or choice == 'd':
    print_days(convert_to_days())
  elif choice == 'H' or choice == 'h':
    print_hours(convert_to_hours())
  elif choice == 'S' or choice == 's':
    print_seconds(convert_to_seconds())
  else:
    retry()

"""Describe this function...
"""
def convert_to_months():
  global months, days, hours, seconds, age, choice
  months = age * 12
  return months

"""Describe this function...
"""
def print_months(months):
  global days, hours, seconds, age, choice
  print(''.join([str(x) for x in ['You are ', months, ' months old.']]))

"""Describe this function...
"""
def convert_to_days():
  global months, days, hours, seconds, age, choice
  days = age * 365
  return days

"""Describe this function...
"""
def print_days(days):
  global months, hours, seconds, age, choice
  print(''.join([str(x2) for x2 in ['You are ', days, ' days old.']]))

"""Describe this function...
"""
def convert_to_hours():
  global months, days, hours, seconds, age, choice
  hours = age * 8760
  return hours

"""Describe this function...
"""
def print_hours(hours):
  global months, days, seconds, age, choice
  print(''.join([str(x3) for x3 in ['You are ', hours, ' hours old.']]))

"""Describe this function...
"""
def convert_to_seconds():
  global months, days, hours, seconds, age, choice
  seconds = age * 31536000
  return seconds

"""Describe this function...
"""
def print_seconds(seconds):
  global months, days, hours, age, choice
  print(''.join([str(x4) for x4 in ['You are ', seconds, ' seconds old.']]))

"""Describe this function...
"""
def retry():
  global months, days, hours, seconds, age, choice
  print('Please run the program again and choose M, D, H, or S.')


main()