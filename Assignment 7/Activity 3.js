//This program wil take a user's age and convert it to their choice of months, days, hours, or seconds.
var months, days, hours, seconds, age, choice;

/**
 * Describe this function...
 */
function main() {
  age = parseFloat(window.prompt('How old are you in years?'));
  choice = window.prompt('Would you like to convert your age to months(M), days(D), hours(H), or seconds(S)?');
  if (choice == 'M' || choice == 'm') {
    print_months(convert_to_months());
  } else if (choice == 'D' || choice == 'd') {
    print_days(convert_to_days());
  } else if (choice == 'H' || choice == 'h') {
    print_hours(convert_to_hours());
  } else if (choice == 'S' || choice == 's') {
    print_seconds(convert_to_seconds());
  } else {
    retry();
  }
}

/**
 * Describe this function...
 */
function convert_to_months() {
  months = age * 12;
  return months;
}

/**
 * Describe this function...
 */
function print_months(months) {
  window.alert(['You are ',months,' months old.'].join(''));
}

/**
 * Describe this function...
 */
function convert_to_days() {
  days = age * 365;
  return days;
}

/**
 * Describe this function...
 */
function print_days(days) {
  window.alert(['You are ',days,' days old.'].join(''));
}

/**
 * Describe this function...
 */
function convert_to_hours() {
  hours = age * 8760;
  return hours;
}

/**
 * Describe this function...
 */
function print_hours(hours) {
  window.alert(['You are ',hours,' hours old.'].join(''));
}

/**
 * Describe this function...
 */
function convert_to_seconds() {
  seconds = age * 31536000;
  return seconds;
}

/**
 * Describe this function...
 */
function print_seconds(seconds) {
  window.alert(['You are ',seconds,' seconds old.'].join(''));
}

/**
 * Describe this function...
 */
function retry() {
  window.alert('Please run the program again and choose M, D, H, or S.');
}


main();