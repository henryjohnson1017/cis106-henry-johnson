#This program reads the plant_catalog XML file and creates a list of the fields for each plant
import xml.etree.ElementTree as ET 

#Reads and returns Catalog
def open_file(filename):
    tree = ET.parse(filename)
    root = tree.getroot()
    return root

#builds parallel lists for each item under each Plant, removing $ from price and converting it into a float
def read_file(root):
    names = []
    botanicals = []
    zones = []
    lights = []
    prices = []
    for x in root.findall("PLANT"):
        name = x.find("COMMON").text
        names.append(name)
        botanical = x.find("BOTANICAL").text
        botanicals.append(botanical)
        zone = x.find("ZONE").text
        zones.append(zone)
        light = x.find("LIGHT").text
        lights.append(light)
        price = x.find("PRICE").text[1:]
        prices.append(float(price))
        print(price)
    return (names, botanicals, zones, lights, prices)
    
    
#this function loops through parallel lists and prints the nth element of each list
def data_list(names, botanicals, zones, lights, prices):
    for n, b, z, l, p in zip(names, botanicals, zones, lights, prices):
        print(n + " (" + b + ") - " + z + " - "+ l + " - " + "$" + str(p))

#counts the number of names of plants
def total_items(names):
    number_items = len(names)
    return number_items
    
#calculates the average price of all plants
def average_price(prices):
    avg_price = sum(prices)/len(prices)
    return avg_price
    
#displays total number of plants and average price per plant
def display_totals(number_items, avg_price):
    print(str(number_items) + " items - $" + str(round(avg_price,2)) + " average price")


def main():
    root = open_file("plant_catalog.xml")
    names, botanicals, zones, lights, prices = read_file(root)
    data_list(names, botanicals, zones, lights, prices)
    number_items = total_items(names)
    avg_price = average_price(prices)
    display_totals(number_items, avg_price)

main()
