# This program calculates a user's age in months, days, hours, and seconds.


"""The main function exucutes the other functions to calculate age
"""
def main():
    age = get_age()
    months = calculate_months(age)
    days = calculate_days(age)
    hours = calculate_hours(days)
    seconds = calculate_seconds(hours)
    print_age(months, days, hours, seconds)


def get_age():
    age = float(input('How many years old are you?'))
    return age
    
"""This function converts the user's age to months."""
def calculate_months(age):
    months = age * 12
    return months

"""This function converts the user's age to days."""
def calculate_days(age):
    days = age * 365
    return days
   
"""This function converts the user's age to hours."""
def calculate_hours(days):
    hours = days * 24
    return hours
    
"""This function converts the user's age to seconds."""
def calculate_seconds(hours):
    seconds = hours * 60 * 60
    return seconds


def calculate_months(age):
    months = age * 12
    return months
    

def print_age(months, days, hours, seconds):
    print('You are about', months, 'months,', days, 'days,', hours, 'hours, or', seconds, 'seconds old.')
    


main()