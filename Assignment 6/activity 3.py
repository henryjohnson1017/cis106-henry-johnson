##This program converts a distance in miles to yards, feet, and inches.

def main():
    mike()

def mike():
    miles = get_miles()
    yards = calculate_yards(miles)
    feet = calculate_feet(miles)
    inches = calculate_inches(miles)
    print_distance(yards, feet, inches)

"""This function prompts the user asking how many miles to convert"""    
def get_miles():
    miles = float(input('How many miles do you wish to convert?'))
    return miles

"""This function converts miles into yards, feet, and inches""" 
def calculate_yards(miles):
    yards = miles * 1760
    return yards
    
"""This function converts miles into feet"""   
def calculate_feet(miles):
    feet = miles * 5280
    return feet
    
"""This function converts miles into yards, feet, and inches""" 
def calculate_inches(miles):
    inches = miles * 63360
    return inches

"""This function prints the converted distances"""  
def print_distance(yards, feet, inches):
    print('This distance converts to ', yards, ' yards, ', feet, ' feet, or ', inches, ' inches.')
    

main()