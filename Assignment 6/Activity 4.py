##This program calculates the area for a rectangle, triangle, trapezoid, and a circle
import math

def main():
    rectangleb1 = get_rectangleb1()
    rectangleh1 = get_rectangleh1()
    rectangle_area = calculate_rectangle_area(rectangleb1, rectangleh1)
    print_rectangle_area(rectangle_area)
    triangleb1 = get_triangleb1()
    triangleh1 = get_triangleh1()
    triangle_area = calculate_triangle_area(triangleb1, triangleh1)
    print_triangle_area(triangle_area)
    trapb1 = get_trapb1()
    trapb2 = get_trapb2()
    traph1 = get_traph1()
    trap_area = calculate_trap_area(trapb1, trapb2, traph1)
    print_trap_area(trap_area)
    circle_radius = get_radius()
    circle_area = calculate_circle_area(circle_radius)
    print_circle_area(circle_area)
    

#################RECTANGLE####################
"""This set of functions calculates, returns, and prints the area of a rectangle"""

"""This function prompts the user for the base of the rectangle"""  
def get_rectangleb1():
    rectangleb1 = float(input('How many units is the base of the rectangle?'))
    return rectangleb1

"""This function prompts the user for the height of the rectangle"""  
def get_rectangleh1():
    rectangleh1 = float(input('How many units is the height of the rectangle?'))
    return rectangleh1

"""This function calculates the area of the rectangle"""
def calculate_rectangle_area(rectangleb1, rectangleh1):
    rectangle_area = rectangleb1 * rectangleh1 
    return rectangle_area

"""This function prints the area of the rectangle"""
def print_rectangle_area(rectangle_area):
    print('The area of the rectangle is ', rectangle_area )
    
###############TRIANGLE#########################  
"""This set of functions calculates, returns, and prints the area of a triangle"""

"""This function prompts the user for the base of the triangle"""  
def get_triangleb1():
    triangleb1 = float(input('How many units is the base of the triangle?'))
    return triangleb1
    
"""This function prompts the user for the height of the triangle"""  
def get_triangleh1():
    triangleh1 = float(input('How many units is the height of the triangle?'))
    return triangleh1

"""This function calculates the area of the triangle"""
def calculate_triangle_area(triangleb1, triangleh1):
    triangle_area = triangleb1 * triangleh1 * 0.5
    return triangle_area

"""This function prints the area of the triangle"""
def print_triangle_area(triangle_area):
    print('The area of the triangle is ', triangle_area )
    
##############TRAPEZOID########################
"""This set of functions calculates, returns, and prints the area of a trapezoid"""

"""This function prompts the user for the base1 of the trapezoid"""  
def get_trapb1():
    trapb1 = float(input('How many units is base1 of the trapezoid?'))
    return trapb1

"""This function prompts the user for the base2 of the trapezoid"""  
def get_trapb2():
    trapb2 = float(input('How many units is base2 of the trapezoid?'))
    return trapb2
    
"""This function prompts the user for the height of the trapezoid"""  
def get_traph1():
    traph1 = float(input('How many units is the height of the trapezoid?'))
    return traph1

"""This function calculates the area of the trapezoid"""
def calculate_trap_area(trapb1, trapb2, traph1):
    trap_area = ((.5 * (trapb1 + trapb2)) * traph1)
    return trap_area

"""This function prints the area of the trapezoid"""
def print_trap_area(trap_area):
    print('The area of the trapezoid is ', trap_area )
    
############CIRCLE#############################
"""This set of functions calculates, returns, and prints the area of a circle"""

"""This function prompts the user for th radius of the circle"""
def get_radius():
    circle_radius = float(input('How many units is the radius of the circle?'))
    return circle_radius

"""This function calculates the area of the circle"""
def calculate_circle_area(circle_radius):
    circle_area = (math.pi * math.pow(circle_radius, 2))
    return circle_area

"""This function prints the area of the circle"""
def print_circle_area(circle_area):
    print('The area of the circle is ', circle_area )
    

main()