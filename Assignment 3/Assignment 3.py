##This program will calculate gross pay using Python

hours = None
hrs = None
rate = None
rt = None
grossPay = None

def text_prompt(msg):
  try:
    return raw_input(msg)
  except NameError:
    return input(msg)


hours = 'How many hours have you worked?'
hrs = float(text_prompt(hours))
rate = 'What is your hourly rate in Dollars?'
rt = float(text_prompt(rate))
grossPay = rt * hrs
print(str('Your Gross Pay is $') + str(grossPay))