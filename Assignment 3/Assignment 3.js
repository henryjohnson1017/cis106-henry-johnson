//This program will calculate gross pay using JavaScript

var age, months, days, hours, seconds;

age = parseFloat(input('How many years old are you?'));

months = age * 12;
days = months * 30;
hours = days * 24;
seconds = hours * 86400;

output(['You are about ', months,' months, ', days,' days, ', hours,' hours, or ', seconds,' seconds old.'].join(''));

function input(text) {
  if (typeof console === 'object') {
    return prompt(text)
  }
  else {
    output(text);
    var isr = new java.io.InputStreamReader(java.lang.System.in); 
    var br = new java.io.BufferedReader(isr); 
    var line = br.readLine();
    return line.trim();
  }
}

function output(text) {
  if (typeof console === 'object') {
    console.log(text);
  } 
  else if (typeof document === 'object') {
    document.write(text);
  } 
  else {
    print(text);
  }
}
