##This program uses a while and for loop to ask a user how many scores they would like to calculate and averages scores

var number_scores, running_total, avg_score, count, score;

/**
 * Describe this function...
 */
function number_prompt() {
  number_scores = parseFloat(window.prompt('How many scores would you like to average?'));
  return number_scores;
}

/**
 * Describe this function...
 */
function main() {
  number_scores = number_prompt();
  running_total = while_loop(number_scores);
  avg_score = calculate_score(number_scores, running_total);
  display_average_score(avg_score);
  number_scores = number_prompt();
  running_total = for_loop(number_scores);
  avg_score = calculate_score(number_scores, running_total);
  display_average_score(avg_score);
}

/**
 * Describe this function...
 */
function while_loop(number_scores) {
  count = 0;
  running_total = 0;
  while (count < number_scores) {
    score = parseFloat(window.prompt('What is your score?'));
    running_total = running_total + score;
    count = (typeof count == 'number' ? count : 0) + 1;
  }
  return running_total;
}

/**
 * Describe this function...
 */
function for_loop(number_scores) {
  count = 0;
  running_total = 0;
  var count_inc = 1;
  if (1 > number_scores) {
    count_inc = -count_inc;
  }
  for (count = 1; count_inc >= 0 ? count <= number_scores : count >= number_scores; count += count_inc) {
    score = parseFloat(window.prompt('What is your score?'));
    running_total = running_total + score;
  }
  return running_total;
}

/**
 * Describe this function...
 */
function calculate_score(number_scores, running_total) {
  avg_score = running_total / number_scores;
  return avg_score;
}

/**
 * Describe this function...
 */
function display_average_score(avg_score) {
  window.alert(String('Your average score is ') + String(avg_score));
}


main();