##This program uses a while and for loop to ask a user how many scores they would like to calculate and averages scores

from numbers import Number

number_scores = None
running_total = None
avg_score = None
count = None
score = None

def text_prompt(msg):
  try:
    return raw_input(msg)
  except NameError:
    return input(msg)

"""Describe this function...
"""
def number_prompt():
  global number_scores, running_total, avg_score, count, score
  number_scores = float(text_prompt('How many scores would you like to average?'))
  return number_scores

"""Describe this function...
"""
def main():
  global number_scores, running_total, avg_score, count, score
  number_scores = number_prompt()
  running_total = while_loop(number_scores)
  avg_score = calculate_score(number_scores, running_total)
  display_average_score(avg_score)
  number_scores = number_prompt()
  running_total = for_loop(number_scores)
  avg_score = calculate_score(number_scores, running_total)
  display_average_score(avg_score)

"""Describe this function...
"""
def while_loop(number_scores):
  global running_total, avg_score, count, score
  count = 0
  running_total = 0
  while count < number_scores:
    score = float(text_prompt('What is your score?'))
    running_total = running_total + score
    count = (count if isinstance(count, Number) else 0) + 1
  return running_total

def upRange(start, stop, step):
  while start <= stop:
    yield start
    start += abs(step)

def downRange(start, stop, step):
  while start >= stop:
    yield start
    start -= abs(step)

"""Describe this function...
"""
def for_loop(number_scores):
  global running_total, avg_score, count, score
  count = 0
  running_total = 0
  for count in (1 <= float(number_scores)) and upRange(1, float(number_scores), 1) or downRange(1, float(number_scores), 1):
    score = float(text_prompt('What is your score?'))
    running_total = running_total + score
  return running_total

"""Describe this function...
"""
def calculate_score(number_scores, running_total):
  global avg_score, count, score
  avg_score = running_total / number_scores
  return avg_score

"""Describe this function...
"""
def display_average_score(avg_score):
  global number_scores, running_total, count, score
  print(str('Your average score is ') + str(avg_score))


main()