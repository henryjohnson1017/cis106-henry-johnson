## This program will convert miles to yards, feet, and inches

miles = float(input('How many miles would you like to convert?'))

yards = miles * 1760
feet = yards * 3
inches = feet * 12

print('This converts to', yards, 'yards,', feet, 'feet, or', inches, 'inches.')
