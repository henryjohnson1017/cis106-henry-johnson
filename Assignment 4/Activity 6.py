## https://docs.python.org/3/library/math.html
## This program calculates the area for a rectangle, triangle, trapezoid, and a circle

import math

triangleb1 = float(input('How many units is the base of the triangle?'))
triangleh1 = float(input('How many units is the height of the triangle?'))
triangleArea = triangleh1 * triangleb1 * 0.5
print(str('The area of the triangle is ') +str(triangleArea))
    
rectangleb1 = float(input('How many units is the base of the rectangle?'))
rectangleh1 = float(input('How many units is the height of the rectangle?'))
rectangleArea = (rectangleb1 * rectangleh1)
print(str('The area of the rectangle is ') +str(rectangleArea))

trapb1 = float(input('How many units is base1 of the trapezoid?'))
trapb2 = float(input('How many units is base2 of the trapezoid?'))
traph1 = float(input('How many units is height of the trapezoid?'))
trapArea = ((.5 * (trapb1 + trapb2)) * traph1)
print(str('The area of the trapezoid is ') +str(trapArea))

circleRadius = float(input('How many units is the radius of the circle?'))
circleArea = (math.pi * math.pow(circleRadius, 2))
print(str('The area of the circle is ') +str(circleArea))
