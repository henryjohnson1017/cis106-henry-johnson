## This program will approximately calculate your age in months, days, hours and seconds.

age = float(input('How many years old are you?'))

months = age * 12
days = age * 365
hours = days * 24
seconds = hours * 60 * 60

print('You are about', months, 'months,', days, 'days,', hours, 'hours, or', seconds, 'seconds old.')
