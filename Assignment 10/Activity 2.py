# Comments go here...


def times_prompt():
    times = float(input("How many times would you like to multiply this number"))
    return times


def number_prompt():
    number = float(input("What is the number you would like to calculate for?"))
    return number


def while_loop():
    times = times_prompt()
    number = number_prompt()
    count = 1
    while count <= times:
        total = calculate_total(count, number)
        display_calculation(count, number, total)
        count = count + 1


def for_loop():
    times = times_prompt()
    number = number_prompt()
    for count in range(1, int(times) + 1):
        total = calculate_total(count, number)
        display_calculation(count, number, total)


def calculate_total(count, number):
    total = count * number
    return total


def display_calculation(count, number, total):
    print(count, "*", number, "=", total)


def main():
    while_loop()
    for_loop()


main()
