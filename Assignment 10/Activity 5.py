# Comments go here...


def get_answer(guess):
    answer = raw_input("Is your number (h)igher, (l)ower, or (e)qual to {}? ".format(guess))
    return answer
    
    
def guess_loop_while():
    low = 1
    high = 100
    guess = 50
    count = 0
    answer = 0
    while answer != guess:
        user_response = get_answer(guess)
        if user_response == "h":
            low = guess
        elif user_response == "l":
            high = guess
        elif user_response == "e":
            answer = guess
        guess = (low + high) // 2
        count = count + 1
    print("your number is ", guess)
    print("This process took", count, "tries to guess.")


def main():
    guess_loop_while()
    

main()
