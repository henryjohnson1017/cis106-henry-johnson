#This program is a guessing game that allows a user to think of a number and input whether the programs guesses are higher, lower, or equal to their number.  
#The program will print the number when indicated it is correct with how many guesses it took, as well as a list of all of the guesses

def get_answer(guess):
    answer = input("Is your number (h)igher, (l)ower, or (e)qual to {}? ".format(guess))
    return answer
    
    
def guess_loop_while():
    list_a = []
    low = 1
    high = 100
    guess = 50
    count = 0
    answer = 0
    while answer != guess:
        list_a.append(guess)
        user_response = get_answer(guess)
        if user_response == "h":
            low = guess
        elif user_response == "l":
            high = guess
        elif user_response == "e":
            answer = guess
        guess = (low + high) // 2
        count = count + 1
    print("your number is ", guess)
    print("This process took", count, "tries to guess.")
    print(list_a)


def main():
    guess_loop_while()
    

main()
