
def determine_if_leap(year, days_number, days_number_leap, month_name):
    if (year % 4) == 0:
        if (year % 100) == 0:
            if (year % 400) == 0:
                print("{0} is a leap year".format(year))
                month = month_lookup(days_number_leap, month_name)
            else:
                print("{0} is not a leap year".format(year))
                month = month_lookup(days_number, month_name)
        else:
            print("{0} is a leap year".format(year))
            month = month_lookup(days_number_leap, month_name)
    else:
        print("{0} is not a leap year".format(year))
        month = month_lookup(days_number, month_name)
        
        
def input_year():
    year = int(input("What year would you like to calculate for? "))
    return year
    
def month_lookup(days_number, month_name):
    month = 6
    while month >= 1 or month <= 12:
        month = int(input("What month would you like to lookup? "))
        month_return(month, days_number, month_name)
    return month
    
def month_lookup_leap(days_number_leap, month_name):
    month = 6
    while month >= 1 or month <= 12:
        month = int(input("What month would you like to lookup? "))
        month_return_leap(month, days_number_leap, month_name)
    return month
    
def month_return(month, days_number, month_name):
    print(str("there are ") + str(days_number[month-1]) + str(" days in ") + str(month_name[month-1]))
  
    
def month_return_leap(month, days_number_leap, month_name):
    print(str("there are ") + str(days_number_leap[month-1]) + str(" days in ") + str(month_name[month-1]))
    
    
    
def main(): 
    days_number = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    days_number_leap = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
    month_name = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']
    year = input_year()
    determine_if_leap(year, days_number, days_number_leap, month_name)
    
    
main()
    
