
def name_input():
    name = input("Input your first and last name")
    return name
    
def concat(name):
    split_name = name.split()
    first_name = split_name[0]
    last_name = split_name[1]
    first_initial = first_name[0]
    return last_name, first_initial

def print_name(last_name, first_initial): #last_name, first_initial
    print(str(last_name + str(",")), str(first_initial + str(".")))
    #print(first_initial)
    
def main():
    name = name_input()
    last_name, first_initial = concat(name)
    print_name(last_name, first_initial)
    
main()
    