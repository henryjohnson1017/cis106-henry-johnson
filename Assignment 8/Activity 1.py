##Create a program to prompt the user for hours and rate per hour to compute gross pay (hours * rate). Include a calculation to give 1.5 times the hourly rate for any overtime (hours worked above 40 hours)

#rate = None
#hours = None
#gross_pay = None

        
def get_hours():
    print("How many hours did you work?")
    hours = input()
    return hours
    
def get_rate():
    print("What is your hourly rate in Dollars?")
    rate = input()
    return rate
    
def regular_hours(rate, hours):
    gross_pay = rate * hours
    return gross_pay
    
def display_regular_hours(gross_pay):
    print("Your gross pay is $" + str(gross_pay))
    
def overtime_hours(rate, hours):
    gross_pay = ((rate * 1.5) * (hours-40)) + (rate * 40)
    return gross_pay

def display_overtime_hours(gross_pay):
    print("Your gross pay is $" + str(gross_pay))
    
def main():
    hours = get_hours()
    rate = get_rate()
    if hours > 40:
        display_overtime_hours(overtime_hours(rate, hours))
    elif hours <= 40:
        display_regular_hours(regular_hours(rate, hours))
    else:
        print("enter a number")
        
main()