##Create a program that asks the user for a distance in miles, and then ask the user if they want the distance in US measurements (yards, feet, and inches) or in metric measurements (kilometers, meters, and centimeters)
def main():
    miles = get_miles()
    choice = get_choice()
    if choice == "U": 
        display_american(miles)
    elif choice == "M":
        display_metric(miles)
    else:
        print ("you must pick a metric to convert to")
 
def text_prompt(msg):
  try:
    return raw_input(msg)
  except NameError:
    return input(msg)
   
def get_choice():
    choice =  text_prompt("Do you want to convert your distance into US(U) or Metric(M)?")
    return choice
    
def display_american(miles):
    yards = calculate_yards(miles)
    feet = calculate_feet(miles)
    inches = calculate_inches(miles)
    print_distance(yards, feet, inches)

def display_metric(miles):
    kilometers = calc_kilometers(miles)
    meters = calc_meters(kilometers)
    centimeters = calc_centimeters(meters)
    print_mdistance(kilometers, meters, centimeters)

"""This function prompts the user asking how many miles to convert"""    
def get_miles():
    miles = float(input('How many miles do you wish to convert?'))
    return miles
 
def calc_kilometers(miles):
    kilometers = miles * 1.609344
    return kilometers

def calc_meters(kilometers):
    meters = kilometers * 1000
    return meters
    
def calc_centimeters(meters):
    centimeters = meters * 100
    return centimeters

def print_mdistance(kilometers, meters, centimeters):
    print('This distance converts to ', kilometers, ' kilometers, ', meters, ' meters, or ', centimeters, ' centimeters.')
    

"""This function converts miles into yards, feet, and inches""" 
def calculate_yards(miles):
    yards = miles * 1760
    return yards
    
"""This function converts miles into feet"""   
def calculate_feet(miles):
    feet = miles * 5280
    return feet
    
"""This function converts miles into yards, feet, and inches""" 
def calculate_inches(miles):
    inches = miles * 63360
    return inches

"""This function prints the converted distances"""  
def print_distance(yards, feet, inches):
    print('This distance converts to ', yards, ' yards, ', feet, ' feet, or ', inches, ' inches.')
    

main()